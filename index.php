<?php
ini_set('display_errors', '1');
date_default_timezone_set('Europe/Berlin');
session_start();

require 'functions.php';
require 'ZabbixApi.class.php'; // load ZabbixApi
use ZabbixApi\ZabbixApi;

$config = parse_config('config.ini');
try {
  $_SESSION['api'] = new ZabbixApi($config['zabbix_url'], $config['zabbix_user'], $config['zabbix_password']);
} catch(Exception $e) {
    echo $e->getMessage();
}
//pprint($config);
$data = array();
$data['triggers'] = get_trigger($config);
require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array(
    'cache' => 'compilation_cache',
    'debug' => true,
));
require_once 'TwigCustomFilter.php';
$template = $twig->loadTemplate('index.html');
$data['refresh'] = $config['refresh'];
$data['title']   = $config['title'];
foreach($config as $key => $value) {
  // Push all config starting with given string to the data passed to the template
  if(preg_match("/^display_/",$key)) {
    $data[$key] = $value;
  }
}
//pprint($data);
echo $template->render($data);
