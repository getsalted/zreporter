<?php
$filter = new Twig_SimpleFilter('priorities', function ($num) {
    if(!is_numeric($num)) {
      die('Input passed to filter must be numeric.');
    }
    $s = array( 1 => 'disaster',
                2 => 'high',
                3 => 'avarage',
                4 => 'warning',
                5 => 'information',
                6 => 'undefined' );
    return $s[$num];
});
$twig->addFilter($filter);


$filter = new Twig_SimpleFilter('faIcon', function ($num) {
    if(!is_numeric($num)) {
      die('Input passed to filter must be numeric.');
    }
    $icon[6] = 'fa-bell';
  	$icon[5] = 'fa-bolt';
	  $icon[4] = 'fa-bell-o';
	  $icon[3] = 'fa-bell-o';
	  $icon[2] = 'fa-exclamation-triangle';
	  $icon[1] = 'fa-info-circle';
	  $icon[0] = 'fa-wrench';
    return $icon[$num];
});
$twig->addFilter($filter);

$filter = new Twig_SimpleFilter('itemvalue', function ($input) {
    if(preg_match("/^(\d+)\s([A-Za-z]+)$/",$input,$match)) {
      $value = $match[1];
      $unit  = $match[2];
    } else {
      return $input;
    }
    switch($unit){
      case "unixtime":
        return date("d.m.Y G:i:s",$value);

      case "B":
        return $input;

      default:
        return $input;
    }
});
$twig->addFilter($filter);
