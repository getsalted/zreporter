<?php

/**
 * Pretty print an array
 * @param  array $data input
 * @return string       formated output
 */
function pprint($data) {
	print "<pre>".print_r($data,TRUE)."</pre>\n";
}

/**
 * Parse Priority string from config
 * @param  string $data Input list of priorities
 * @return array priorities
 */
function parse_priorities($data){
	$data = str_replace(" ", "", $data);
	if( preg_match("/^(1|2|3|4|5|6|,)+$/u", $data)) {
			return(explode(',', $data));
	}
	else {
		die("Priority can only contain numbers from 1 to 6 delimited by commas.");
	}
}

/**
 * parse an ini file and generate config
 * @param  string $file location of config file
 * @return array       configuration
 */
function parse_config($file) {
	$ini = parse_ini_file($file,TRUE);
	if( isset($_GET['conf']) && array_key_exists($_GET['conf'],$ini)) {
		$config = $ini[$_GET['conf']] + $ini['global'];
	} else {
		$config = $ini['global'];
	}
	if(array_key_exists('priorities', $config)){
		$config['priorities'] = parse_priorities($config['priorities']);
	}
	return $config;
}


/**
 * format a date string
 * @param  string $date date input
 * @return string       formated output
 */
function format_date($date) {
	if(date('Y-m-d',$date) == date('Y-m-d')) return "heute ";
    if(date('Y-m-d',$date) == date('Y-m-d',(time()-86400))) return "gestern ";
    return date('d.m.Y',$date);
}


function format_clock($date) {
	return date('G:i',$date)." h";
}

/**
 * Check wether a trigger is akcknowledged or not
 * @param  int $trigger_id trigger_id
 * @return object             stdClass with status of acklnowledgement
 */
function get_acknowledgment($trigger_id)
{
	$options = array(	"output" 			      	=> "extend",
				            "select_acknowledges"	=> "extend",
				             "objectids"	   			=> $trigger_id,
				             "sortfield"	  			=> array("clock", "eventid"),
				             "sortorder"	  			=> "DESC",
				              //"limit"             => '1'
				    );
	try
    {
        $events = $_SESSION['api']->eventGet($options);
				foreach($events as $Oevent) {
					$event = get_object_vars($Oevent);
					if($event['acknowledged'] == 0) {
						continue;
					}
					$ack = get_object_vars($event['acknowledges'][0]);
					$ack['message'] = str_replace('----[BULK ACKNOWLEDGE]----', '',$ack['message']);
					$ack['ackInitials'] = strtoupper(substr($ack['name'], 0,1).substr($ack['surname'], 0,1));
					$return[$event['objectid']] = $ack;
				}
        return $return;
    }
    catch (Exception $e)
    {
        echo $e->getMessage();
        return NULL;
    }
}

function play_sound($config)
{
    if(! $config['play_sound']) return NULL;
    if($_SESSION['last_beep'] && $_SESSION['last_beep'] > time()-600 ) return NULL;
    $_SESSION['last_beep'] = time();

    $playing = TRUE;
    echo '<object id="player">
            <param name="autostart" value="true">
            <param name="src" value="beep.wav">
            <param name="autoplay" value="true">
            <param name="controller" value="true">
            <embed src="beep.wav" controller="false" autoplay="true" autostart="True" type="audio/wav" />
           </object>';
}

/**
 * get items and values from list of itemids
 * @param  array $item_ids
 * @return array
 */
function get_items($item_ids){
	$options = array(	'itemids' => $item_ids,
							       'output' => 'extend');
	try{
		foreach( $_SESSION['api']->itemGet($options) as $Oitem ) {
			$item = get_object_vars($Oitem);
			$return[$item['itemid']] = $item;
			//pprint($Oitem);
		}
		return $return;
	}
	catch(Exception $e) {
	    echo $e->getMessage();
	}
}

/**
 * Get all triggers aka reported problems from zabbix
 * @param  array $config parsed config file
 * @return array         list of triggers
 */
function get_trigger($config) {
	try {
	    $options = array(   'monitored'         =>TRUE,
	                        'active'            =>TRUE,
	                        'only_true'         =>TRUE,
	                        'expandData'        =>TRUE,
	                        'expandDescription' => TRUE,
	                        'expandExpression'  => FALSE,
	                        'selectFunctions'   => 'extend',
	                        'output'            => 'extend',
	                        'sortfield'         => 'priority',
	                        'sortorder'         => 'ASC',
	                        'filter'            => array('value' => '1') );

	    $triggers = array();
	    $ack_triggers = array();
	    foreach( $_SESSION['api']->triggerGet($options) as $Otrigger ) {
	        $trigger = get_object_vars($Otrigger);
					$trigger['itemid'] = $trigger['functions'][0]->itemid;
					unset($trigger['functions']);
					$trigger_ids[] = $trigger['triggerid'];
					$item_ids[] = $trigger['itemid'];
					$triggers[] = $trigger;
	    }
			if($config['display_item'] == 1){
				$items = get_items($item_ids);
				// Add some item data to the trigger array
				foreach($triggers as $no => $trigger){
					$id = $trigger['itemid'];
					if(array_key_exists($id, $items)){
						$triggers[$no]['itemname']   = $items[$id]['name'];
						$triggers[$no]['itemkey']    = $items[$id]['key_'];
						$triggers[$no]['itemstatus'] = $items[$id]['status'];
						$triggers[$no]['itemvalue']  = $items[$id]['lastvalue'];
						$triggers[$no]['itemunit']   = $items[$id]['units'];
						$triggers[$no]['itemvalueu'] = $items[$id]['lastvalue']." ".$items[$id]['units'];
						$triggers[$no]['itemdesc']   = $items[$id]['description'];
					}
				}
				//pprint($triggers); die();
			}
			if($config['display_ack'] == 0){
				return $triggers;
			}

			$ack = get_acknowledgment($trigger_ids);
	    foreach($triggers as $no => $trigger){
				if(array_key_exists($trigger['triggerid'], $ack)){
					if($config['ack_on_bottom'] == 1){
						$ack_triggers[$no] = $trigger + $ack[$trigger['triggerid']];
						unset($triggers[$no]);
					} else {
						$triggers[$no] = $triggers[$no] + $ack[$trigger['triggerid']];
					}
				}

			}
			//pprint($ack_triggers);
			$return = $triggers + $ack_triggers;
			//pprint($return);
		  return $return;
	}
	catch(Exception $e) {
	    echo $e->getMessage();
	}
}
